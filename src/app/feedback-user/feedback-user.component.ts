import { Component, OnInit,  } from '@angular/core';
import { Router } from '@angular/router';
import { ControlService } from '../services/control.service';
import { ProfileService } from '../services/profile.service';
import { EmailService } from '../services/email.service';
import { DialogComponent } from '../dialog/dialog.component';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-feedback-user',
  templateUrl: './feedback-user.component.html',
  styleUrls: ['./feedback-user.component.scss']
})
export class FeedbackUserComponent implements OnInit {
  items:any;
  constructor(public router: Router, public dialog: MatDialog, public email: EmailService, public ctrl: ControlService, public ph:ProfileService) { }
  ngOnInit() {
    this.ph._getFeedUserProfile().on('value', snapshot => {
      this.items = [];
    //  this.pop.hideLoader()
      snapshot.forEach( snap => {

        this.items.push({
          key: snap.key,
          email: snap.val().email,
          review: snap.val().complain,
        });
        return false

      
      });
    });
  }





  Open(id): void {
    this.router.navigateByUrl('chat_user')
    this.ctrl.cur_Item = id
  }


}



