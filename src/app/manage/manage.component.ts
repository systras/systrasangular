import { Component, OnInit } from '@angular/core';
import { ProfileService } from '../services/profile.service';
@Component({
  selector: 'app-manage',
  templateUrl: './manage.component.html',
  styleUrls: ['./manage.component.scss']
})
export class ManageComponent implements OnInit {
  form: any;
  snap: any;
  submitted: boolean = false;
  constructor(public ph: ProfileService) { }

  ngOnInit() {
    this.ph.getDashboardProfile().on('value', snapshot => {
     
    
      this.snap = snapshot.val().sharePercentage
       
     

     console.log(this.snap)
   });

  }

  onSubmit(form: any ) {
    this.form = form;
    return this.ph.dashboardProfile.update({
      sharePercentage: form.name,
    }).then(d =>{
      this.submitted = true;
      console.log(d)
    });
  } 

}
