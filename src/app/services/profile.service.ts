import { Injectable } from '@angular/core';

import * as firebase from 'firebase';

@Injectable()
export class ProfileService {

  public driverProfile: any;
  public userProfile: any;
  public activityProfile: any;
  public dashboardProfile: any;
  public ActiveDriverProfile: any;
  public ItemProfile: any;
  public userFeed: any;
  public driverFeed: any;
  public sharingID: any;
  LondonProf: any;
  
  public subitemProfile: any;
    constructor() {
      this.dashboardProfile = firebase.database().ref(`DashboardSettings`);
      this.driverProfile = firebase.database().ref(`driverProfile`);
      this.userFeed = firebase.database().ref(`DashboardSettings/user/complains`);
      this.driverFeed = firebase.database().ref(`DashboardSettings/driver/complains`);
      this.userProfile = firebase.database().ref(`userProfile`);
      this.activityProfile = firebase.database().ref(`Customer`);
      this.ActiveDriverProfile = firebase.database().ref(`Drivers`);
      this.ItemProfile = firebase.database().ref(`ItemProfile`);
      this.LondonProf = firebase.database().ref(`LondonRides`);
      this.sharingID = firebase.database().ref(`SharingID`);
      
      this.subitemProfile = firebase.database().ref(`ItemProfile/Stuffs`);
     }

   
    getActivityProfile(): firebase.database.Reference {
      return this.activityProfile;
    }

    getItemProfile(id: any): firebase.database.Reference {
      return this.ItemProfile.child(id)
    }
    
    getDriverProfile(id: any): firebase.database.Reference {
      return this.driverProfile.child(id)
    }

    getDriverProfileHistory(id: any): firebase.database.Reference {
      return this.driverProfile.child(id).child('eventList');
    }

    _getDriverProfile(): firebase.database.Reference {
      return this.driverProfile
    }

    getUserProfile(id: any): firebase.database.Reference {
      return this.userProfile.child(id)
    }

    getUserProfileHistory(id: any): firebase.database.Reference {
      return this.userProfile.child(id).child('eventList');
    }
  
    _getUserProfile(): firebase.database.Reference {
      return this.userProfile
    }

    _getLondonProfile(): firebase.database.Reference {
      return this.LondonProf
    }

    getLondonProfile(id: any): firebase.database.Reference {
      return this.LondonProf.child(id)
    }

    _getFeedUserProfile2(id): firebase.database.Reference {
      return this.userFeed.child(id)
    }

    _getFeedDriverProfile2(id): firebase.database.Reference {
      return this.driverFeed.child(id)
    }

    _getFeedUserProfile(): firebase.database.Reference {
      return this.userFeed
    }

    getFeedUserProfile(id: any): firebase.database.Reference {
      return this.userFeed.child(id)
    }

    getFeedDriverProfile(id: any): firebase.database.Reference {
      return this.driverFeed.child(id)
    }

    _getFeedDriverProfile(): firebase.database.Reference {
      return this.driverFeed
    }
  
  
    ChangeItem(id: any, idi: any): firebase.database.Reference {
      return firebase.database().ref(`ItemProfile/${id}/Stuffs/${idi}`);
    }

    ChangeMainItem(id: any): firebase.database.Reference {
      return firebase.database().ref(`ItemProfile/${id}/`);
    }

    getInnerItem(id: any): firebase.database.Reference {
      return firebase.database().ref(`ItemProfile/${id}/Stuffs`);
    }
    
    getSecondaryItem(id: any, subid: any): firebase.database.Reference {
      return firebase.database().ref(`ItemProfile/${id}/Stuffs/${subid}`);
    }

    getDashboardProfile(): firebase.database.Reference {
      return this.dashboardProfile;
    }

    getActiveDriverProfile(): firebase.database.Reference {
      return this.ActiveDriverProfile;
    }

    updateState(id): firebase.database.Reference {
      return this.driverProfile.child(id).update({
        active_state: true
      });
    }

}
